import os
import tempfile


class File:
    def __init__(self, filename):
        self.filename = filename
        if not os.path.exists(filename):
            open(filename, 'a').close()
        self.f = None
        self.current = 0
        self.end = 0
        self.lines = []

    def __str__(self):
        return self.filename

    def __iter__(self):
        self.f = open(self.filename, "r")
        self.lines = self.f.readlines()
        self.f.close()
        self.end = len(self.lines)
        return self

    def __next__(self):
        if self.current < self.end:
            result = self.lines[self.current]
            self.current += 1
            return result
        else:
            raise StopIteration

    def read(self):
        self.f = open(self.filename, "r")
        result = self.f.read()
        self.f.close()
        return result

    def write(self, line):
        self.f = open(self.filename, "w")
        result = self.f.write(line)
        return result

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.f.close()

    def __add__(self, other):
        new_file = File(os.path.join(tempfile.gettempdir(), 'myfile.txt'))
        new_file.write(self.read() + other.read())
        return new_file
