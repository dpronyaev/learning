def decorate(func):
    def wrapper(*args):
        print("Декорировано!")
        func(*args)
    return wrapper

@decorate
def multiplier(a, b):
    print(a*b)

multiplier(2,3)