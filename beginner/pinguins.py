import sys

num = int(sys.argv[1])

a = ("   _~_    ")
b = ("  (o o)   ")
c = (" /  V  \  ")
d = ("/(  _  )\\ ")
e = ("  ^^ ^^   ")

print(a * num)
print(b * num)
print(c * num)
print(d * num)
print(e * num)

