even_set = {0, 2, 4, 6, 8}
odd_set = {1, 3, 5, 7}

result_set = even_set | odd_set
print(result_set)

result_set = even_set ^ odd_set
print(result_set)