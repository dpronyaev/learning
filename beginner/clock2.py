import sys
seconds = int(sys.argv[1])

hours = seconds // 3600
seconds = seconds - hours * 3600
minutes = seconds // 60
seconds = seconds - minutes * 60

minutes = "00" + str(minutes)
seconds = "00" + str(seconds)

print(str(hours) + ":" + minutes[-2:] + ":" + seconds[-2:])

