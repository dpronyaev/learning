import asyncio


class ClientServerProtocol(asyncio.Protocol):
    metrics_dict = {}

    def connection_made(self, transport):
        self.transport = transport

    def data_received(self, data):
        resp = self.process_data(data.decode())
        self.transport.write(resp.encode())

    def process_data(self, data):
        temp_data = data.split("\n")
        message = temp_data[0].split(" ")
        if message[0] == "put":
            return self.handle_put(message)
        elif message[0] == "get":
            return self.handle_get(message)
        else:
            return "error\nwrong command\n\n"

    def handle_put(self, command):
        if len(command) != 4:
            return "error\nwrong metric format\n\n"
        else:
            if command[1] not in self.metrics_dict:
                self.metrics_dict[command[1]] = [(command[3], command[2])]
            else:
                # if server gets more then 1 metric per second
                current_timestamp = command[3]
                for value in self.metrics_dict[command[1]]:
                    if value[0] == current_timestamp:
                        return "ok\n\n"
                self.metrics_dict[command[1]].append((command[3], command[2]))
        return "ok\n\n"

    def handle_get(self, command):
        # delete excess symbols \n \r
        key = command[1].replace("\n", "")
        key = key.replace("\r", "")

        # if wrong key specified
        if key != "*" and key not in self.metrics_dict.keys():
            return "error\nwrong command\n\n"

        # get all keys
        if key == "*":
            response_string = "ok\n"
            for key in self.metrics_dict.keys():
                for value in self.metrics_dict[key]:
                    response_string += key + " " + value[1] + " " + value[0] + "\n"
            response_string += "\n\n"
            return response_string

        # get single key
        response_string= "ok\n"
        for value in self.metrics_dict[key]:
            response_string += key + " " + value[1] + " " + value[0] + "\n"
        response_string += "\n\n"
        return response_string


def run_server(host, port):
    loop = asyncio.get_event_loop()
    coro = loop.create_server(
        ClientServerProtocol,
        host, port
    )

    server = loop.run_until_complete(coro)

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass

    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()

run_server('127.0.0.1', 8181)