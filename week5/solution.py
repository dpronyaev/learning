import socket
import time


class ClientError(Exception):
    pass


class Client:
    def __init__(self, addr, port, timeout=None):
        self.addr = addr
        self.port = port
        self.sock = socket.socket()
        self.sock.connect((addr, port))
        if timeout != None:
            self.sock.settimeout(timeout)


    def put(self, *args, timestamp=None):
        # add timestamp
        if timestamp ==  None:
            timestamp = int(time.time())
        else:
            self.timestamp = timestamp

        # form and send message
        message = "put"
        for arg in args:
            message += " " + str(arg)
        message += " " + str(self.timestamp) + "\n"
        self.sock.sendall(message.encode())

        # get response
        try:
            response = self.sock.recv(1024).decode()
            if not response:
                raise ClientError("ClientError")
            return {}
        except ClientError as e:
            print(e)

    def get(self, metric):
        # form and send message
        if metric == '*':
            message = "get *\n"
        else:
            message = 'get {}\n'.format(metric)
        self.sock.sendall(message.encode())

        # get and parse response
        try:
            response = self.sock.recv(1024).decode()
            if not response:
                raise ClientError("No response from server")
            else:
                answers = response.split("\n")
                # if server returns error
                if answers == ['error', 'wrong command', '', '']:
                    raise ClientError("Server returned error: wrong command")

                # if server returns no values
                elif answers == ['ok', '', '']:
                    return {}

                # if server returns some values
                elif answers[0] == 'ok':
                    metrics_dict = {}
                    metrics = answers[1:-3]
                    for metric in metrics:
                        metric = metric.split(" ")
                        try:
                            if len(metric) != 3:
                                raise ClientError("Wrong metric format")
                            else:
                                if metric[0] not in metrics_dict.keys():
                                    metrics_dict[metric[0]] = [(metric[2], metric[1])]
                                else:
                                    metrics_dict[metric[0]].append(((metric[2], metric[1])))

                        except ClientError as e:
                            print(e)
                    return metrics_dict

        except ClientError as e:
            print(e)


client = Client("127.0.0.1", 8181, timeout=15)

# test put
client.put("palm.cpu", 0.5, timestamp=1150864247)
client.put("palm.cpu", 1.5, timestamp=1150864248)
client.put("palm.cpu", 2.5, timestamp=1150864248)
client.put("eardrum.cpu", 3, timestamp=1150864250)
client.put("eardrum.cpu", 4, timestamp=1150864251)
client.put("eardrum.memory", 4200000)

# test get
print(client.get("palm.cpu"))
print(client.get("*"))

# close client socket
client.sock.close()




