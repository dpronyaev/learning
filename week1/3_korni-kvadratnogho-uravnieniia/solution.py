import sys 
from math import sqrt

a = int(sys.argv[1])
b = int(sys.argv[2]) 
c = int(sys.argv[3])

D = b*b - 4 * a * c

root1 = int((-b + sqrt(D)) / (2*a))
root2 = int((-b - sqrt(D)) / (2*a))
print(root1)
print(root2)