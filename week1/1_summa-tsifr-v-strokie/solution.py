import sys

digit_string = sys.argv[1]

# string
sum = 0
for digit in digit_string:
    sum += int(digit)

print(sum)

# int
sum = 0
length = len(digit_string)
current_digits = int(digit_string)
for i in range(length, 0, -1):
    multiplier = int("1" + "0" * (i - 1))
    current_digit = current_digits // multiplier
    current_digits -= current_digit * multiplier
    sum += current_digit

print(sum)