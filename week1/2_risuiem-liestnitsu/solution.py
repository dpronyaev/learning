import sys

space = " "
sign = "#"

steps = int(sys.argv[1])

for i in range(1, steps+1):
    result = ""
    result = space * (steps - i)
    result = result + sign * i
    print(result)
