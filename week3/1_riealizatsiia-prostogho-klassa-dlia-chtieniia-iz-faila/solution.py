class FileReader:
    def __init__(self, filename):
        self.filename = filename

    def read(self):
        try:
            with open(self.filename, "r") as f:
                result = f.read()
                return result
        except FileNotFoundError:
            return ""


reader = FileReader("test.txt")
print(type(reader))
print(reader.read())

reader = FileReader("no_such_file.txt")
print(type(reader))
print(reader.read())