import csv
import os

class CarBase:
    def __init__(self, brand, photo_file_name, carrying):
        self.brand = brand
        self.photo_file_name = photo_file_name
        self.carrying = carrying

    def get_photo_file_ext(self):
        return os.path.splitext(self.photo_file_name)[1]



class Car(CarBase):
    def __init__(self, brand, photo_file_name, carrying, passenger_seats_count):
        super().__init__(brand, photo_file_name, carrying)
        self.passenger_seats_count = passenger_seats_count


class Truck(CarBase):
    def __init__(self, brand, photo_file_name, carrying, body_whl):
        super().__init__(brand, photo_file_name, carrying)
        body_whl_list = body_whl.split('x')
        self.body_length = body_whl_list[0]
        self.body_width = body_whl_list[1]
        self.body_height = body_whl_list[2]

    def get_body_volume(self):
        return float(self.body_length) * float(self.body_width) * float(self.body_height)

class SpecMachine(CarBase):
    def __init__(self, brand, photo_file_name, carrying, extra):
        super().__init__(brand, photo_file_name, carrying)
        self.extra = extra


def get_car_list(csv_filename):
    car_list = []
    with open(csv_filename) as csv_fd:
        reader = csv.reader(csv_fd, delimiter=';')
        next(reader)  # пропускаем заголовок
        for row in reader:
            if len(row) != 7:
                continue

            brand = row[1]
            photo_file_name = row[3]
            carrying = row[5]

            if row[0] == 'car':
                passenger_seats_count = row[2]
                if not brand or not photo_file_name or not carrying or not passenger_seats_count:
                    continue
                else:
                    car_list.append(Car(brand, photo_file_name, carrying, passenger_seats_count))
            elif row[0] == 'truck':
                body_whl = row[4]
                if not brand or not photo_file_name or not carrying or not body_whl or len(body_whl.split('x')) != 3:
                    continue
                else:
                    car_list.append(Truck(brand, photo_file_name, carrying, body_whl))
            elif row[0] == 'spec_machine':
                extra = row[6]
                if not brand or not photo_file_name or not carrying or not extra:
                    continue
                else:
                    car_list.append(SpecMachine(brand, photo_file_name, carrying, extra))
            else:
                continue
    return car_list

# проверка, что у нас вернулось 4 экземпляра и что методы работают
print(len(get_car_list("coursera_week3_cars.csv")))
for car in get_car_list("coursera_week3_cars.csv"):
    print(car.__dict__)
    print(car.get_photo_file_ext())
    if type(car) == Truck:
        print(car.get_body_volume())
