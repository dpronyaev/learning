import json
import functools

def to_json(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return json.dumps(func(*args, **kwargs))
    return wrapper

# функция без параметров
@to_json
def get_data():
    return {
        'data': 42
    }

# функция с параметрами
@to_json
def get_other_data(a,b):
    return {a: b}

print(get_data())  # вернёт '{"data": 42   }'
print(get_other_data(22,33))
