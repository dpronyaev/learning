import os
import json
import tempfile
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--key", help="key name")
parser.add_argument("--val", help="key value")
args = parser.parse_args()

storage_path = os.path.join(tempfile.gettempdir(), 'storage.data')

def add_data(key, value, file):

        file.write(json.dumps({key: [value]}))

def update_data(key, value, file):
    saved_data = json.load(file)
    if key in saved_data:
        saved_values = saved_data.get(key)
        if value not in saved_values:
            saved_values.append(value)
            saved_data.update({key: saved_values})
            file.seek(0)
            file.write(json.dumps(saved_data))
    else:
        saved_data.update({key: [value]})
        file.seek(0)
        file.write(json.dumps(saved_data))


def get_data(key, file):
    saved_data = json.load(file)
    if saved_data.get(key):
        print(', '.join(saved_data.get(key)))
    else:
        print(saved_data.get(key))

def putter():
    if not os.path.exists(storage_path):
        with open(storage_path, 'w') as f:
            add_data(args.key, args.val, f)
    else:
        with open(storage_path, 'r+') as f:
            update_data(args.key, args.val, f)
def getter():
    with open(storage_path, 'r') as f:
        get_data(args.key, f)

if args.key and args.val:
    putter()
elif args.key:
    getter()
